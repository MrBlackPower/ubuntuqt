ARG UBUNTU_VERSION=16
ARG UBUNTU_SUBVERSION=04

FROM ubuntu:${UBUNTU_VERSION}.${UBUNTU_SUBVERSION}

###
#  CREATES USER
###

RUN useradd remote_user
# RUN echo remote_user:123456 | chpasswd

RUN apt-get update && \
    apt-get install -y redis-server && \
    apt-get clean

RUN apt-get update -q && \
    DEBIAN_FRONTEND=noninteractive apt-get install -q -y --no-install-recommends \
    default-jdk \
    libice6 \
    libsm6 \
    libx11-xcb1 \
    libxext6 \
    libxrender1 \
    p7zip \
    xvfb \
	ca-certificates \
	curl \
	python \
	gperf \
	bison \
	flex \
	build-essential \
	pkg-config \
	libgl1-mesa-dev \
	libicu-dev \
	firebird-dev \
	libmysqlclient-dev \
	libpq-dev \
	# bc suggested for openssl tests
	bc \
	libssl-dev \
	# git is needed to build openssl in older versions
	git \
	# xcb dependencies
	libfontconfig1-dev \
	libfreetype6-dev \
	libx11-dev \
	libxext-dev \
	libxfixes-dev \
	libxi-dev \
	libxrender-dev \
	libxcb1-dev \
	libx11-xcb-dev \
	libxcb-glx0-dev \
	libxkbcommon-x11-dev \
	# bash needed for argument substitution in entrypoint
	bash \
	# since 5.14.0 we apparently need libdbus-1-dev and libnss3-dev
	libnss3-dev \
	libdbus-1-dev \
    && apt-get clean

#RUN apt-get install unzip wget -y

RUN apt-get install openssh-server -y
RUN mkdir /var/run/sshd
RUN apt-get update 

#INSTALL QT
ARG QT_VERSION_A=5.12
ARG QT_VERSION_B=5.12.2
ARG CORE_COUNT=8
RUN curl -sSL https://download.qt.io/official_releases/qt/${QT_VERSION_A}/${QT_VERSION_B}/single/qt-everywhere-src-${QT_VERSION_B}.tar.xz | tar xJ

WORKDIR /qt-everywhere-src-${QT_VERSION_B}

RUN chmod +x configure
RUN ./configure -opensource -debug

RUN make

##
# NEVER CHANGE ABOVE THIS
##

RUN make install

ENV PATH="/qt-everywhere-src-${QT_VERSION_B}/qtbase/bin:${PATH}"

## CHANGES USER
USER remote_user
##

ENV PATH="/usr/bin/cmake/bin:${PATH}"

ENV PATH="/qt-everywhere-src-${QT_VERSION_B}/qtbase/bin:${PATH}"

## CHANGER TO ROOT
USER root
##


#INSTALL CMAKE
ARG CMAKE_VERSION=3.18.2
RUN wget https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}-Linux-x86_64.sh \
      -q -O /tmp/cmake-install.sh \
      && chmod u+x /tmp/cmake-install.sh \
      && mkdir /usr/bin/cmake \
      && /tmp/cmake-install.sh --skip-license --prefix=/usr/bin/cmake \
      && rm /tmp/cmake-install.sh

ENV PATH="/usr/bin/cmake/bin:${PATH}"

#RUN chmod ugo+rwx -R /qt-everywhere-src-${QT_VERSION_B}

EXPOSE 22

RUN mkdir /home/remote_user
RUN mkdir /home/remote_user/.ssh &&\
    chmod 700 /home/remote_user/.ssh

RUN mkdir /home/remote_user/git

RUN mkdir /home/remote_user/checkout

WORKDIR /home/remote_user/git

RUN chown remote_user:remote_user -R /home/remote_user/git

COPY remote-key.pub /home/remote_user/.ssh/authorized_keys
COPY remote-key.pub /remote_user/.ssh/authorized_keys

RUN chown remote_user:remote_user -R /home/remote_user/.ssh && \
    chmod 600 /home/remote_user/.ssh/authorized_keys

RUN usermod -aG sudo remote_user

#RUN /usr/sbin/sshd-keygen

RUN apt-get clean&& \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

##OPENGL


RUN apt-get update -q && \
    DEBIAN_FRONTEND=noninteractive apt-get install -q -y --no-install-recommends \
	libfontconfig1 \
	mesa-common-dev \
	libglu1-mesa-dev \
	freeglut3-dev \
	libflann-dev

WORKDIR /

RUN apt-get update && \
      apt-get -y install sudo

RUN adduser remote_user sudo

CMD ["/usr/sbin/sshd", "-D"]